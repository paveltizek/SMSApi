# SMS sluzba PHP wrapper

Wrapper for https://sms.sluzba.cz

## Installation

The recommended way to install SMS api is through composer:

```bash
$ composer require paveltizek/sms-api
```

## Usage

#### Sending simple SMS

```php


$smsApi = new PavelTizek\SMSApi('login', 'password');

$smsApi->setRecipient(['777666111']);
$smsApi->send('Message text');

```

If the request fails during some bad request optionst, 
SMSApi throws exception. If the request is ok, returns 
``` PavelTizek/Response ```