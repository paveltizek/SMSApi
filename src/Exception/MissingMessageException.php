<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: paveltizek
 * Date: 29.10.17
 * Time: 7:31
 */

namespace PavelTizek\SMSApi\Exception;


class MissingMessageException extends SMSApiException
{

}