<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: paveltizek
 * Date: 29.10.17
 * Time: 7:33
 */

namespace PavelTizek\SMSApi\Exception;


class GatewayFailureException extends SMSApiException
{

}