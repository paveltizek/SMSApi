<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: paveltizek
 * Date: 29.10.17
 * Time: 7:10
 */

namespace PavelTizek\SMSApi\Exception;


class SMSApiException extends \Exception
{

}