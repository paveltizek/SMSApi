<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: paveltizek
 * Date: 29.10.17
 * Time: 7:09
 */

namespace PavelTizek\SMSApi;


class Response
{
    /** @var  int */
    private $code;

    /** @var  string */
    private $message;

    /**
     * Response constructor.
     * @param int $code
     * @param string $message
     */
    public function __construct($code, $message)
    {
        $this->code = $code;
        $this->message = $message;
    }

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }




}