<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: paveltizek
 * Date: 29.10.17
 * Time: 6:48
 */

namespace PavelTizek\SMSApi;


use PavelTizek\SMSApi\Exception\BadLoginException;
use PavelTizek\SMSApi\Exception\BadPhoneNumberException;
use PavelTizek\SMSApi\Exception\GatewayFailureException;
use PavelTizek\SMSApi\Exception\InsufficientCreditException;
use PavelTizek\SMSApi\Exception\MissingMessageException;
use PavelTizek\SMSApi\Exception\UnknownActionException;

class SMSApi
{
    private CONST BLOCK_SIZE = 8196;
    private CONST API_URL_POST = 'https://smsgateapi.sms-sluzba.cz/apipost30/sms';
    private CONST API_URL_LITE = 'https://smsgateapi.sms-sluzba.cz/apipost30/sms';
    private const AUTH_MSG_LENGTH = 31;
    private const API_POST = 0;
    private const API_LITE = 1;
    private const ACT = 'send';

    /** @var  string */
    private $login;

    /** @var  string */
    private $password;

    /** @var  string */
    private $message;

    private $url;

    private $recipient;

    /**
     * SMSApi constructor.
     * @param string $login
     * @param string $password
     * @param int|string $method
     */
    public function __construct($login, $password, $method = self::API_POST)
    {
        $this->login = $login;
        $this->password = $password;
        $this->url = $method == self::API_POST ? self::API_URL_POST : self::API_URL_LITE;

    }


    /**
     * @param $message
     * @return Response
     * @throws BadLoginException
     * @throws BadPhoneNumberException
     * @throws GatewayFailureException
     * @throws InsufficientCreditException
     * @throws MissingMessageException
     * @throws UnknownActionException
     */
    public function send($message): ?Response
    {
        $this->message = $message;

        $data = [
            'login' => $this->login,
            'msg' => $this->message,
            'msisdn' => $this->recipient,
            'auth' => $this->getAuthToken(),
            'act' => self::ACT,
        ];

        $ch = curl_init($this->url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, "Content-Type: application/x-www-form-urlencoded; charset=utf-8");
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20); //seconds

        $responseXML = curl_exec($ch);
        curl_close($ch);

        if ($responseXML) {
            $response = new \SimpleXMLElement($responseXML);
        } else {
            return null;
        }


        switch ((string)$response->id) {
            case '401':
                throw new BadLoginException('Bad login');
            case '400':
                throw new UnknownActionException('Unknown actio');
            case '402':
                throw new InsufficientCreditException('Insufficient credit');
            case '503':
                throw new GatewayFailureException('Gateway failure');
            case '400;1':
                throw new BadPhoneNumberException('Bad phone number');
            case '400;2':
                throw new MissingMessageException('Missing message');
            default:
                return new Response(200, 'Success');
        }
    }

    /**
     * @return string
     */
    private function getAuthToken(): string
    {
        return md5(md5($this->password) . $this->login . self::ACT . substr($this->message, 0, self::AUTH_MSG_LENGTH));
    }


    /**
     * @param string $recipient
     */
    public function setRecipient(string $recipient): void
    {
        $this->recipient = $recipient;
    }


}