<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: paveltizek
 * Date: 29.10.17
 * Time: 8:08
 */

namespace PavelTizek\SMSApi\Tests;

require __DIR__ . '/../vendor/autoload.php';


use PavelTizek\SMSApi\Exception\BadLoginException;
use PavelTizek\SMSApi\SMSApi;
use Tester\Assert;
use Tester\Environment;
use Tester\TestCase;

class SMSApiTest extends TestCase
{
    public function testBadLogin()
    {

        $smsApi = new SMSApi('ran', 'dom');
        $smsApi->setRecipient('xxx');

        Environment::setup();
        Assert::exception(function () use ($smsApi){
            $smsApi->send("test");
        } , BadLoginException::class);
    }


}

(new SMSApiTest())->run();